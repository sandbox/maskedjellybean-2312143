<?php
// Plugin definition
$plugin = array(
  'title' => t('Bartlett Flipped'),
  'icon' => 'bartlett-flipped.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'bartlett_flipped_bootstrap',
  'regions' => array(
    'sidebar' => t('Sidebar'),
    'contentheader' => t('Content Header'),
    'contentcolumn1' => t('Content Column 1'),
    'contentcolumn2' => t('Content Column 2'),
  ),
);
