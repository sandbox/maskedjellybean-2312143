<div class="hewston">
  <div class="hewston-top row">
    <div class="hewston-slider col-md-8 bpl-content-header bpl-spacer">
      <?php print $content['slider']; ?>
    </div>
    <div class="hewston-slider-gutter col-md-4 bpl-content-header bpl-spacer">
      <?php print $content['slidergutter']; ?>
    </div>
  </div>
  <div class="hewston-middle row">
    <div class="hewston-col-1 hewston-col col-md-4 bpl-content bpl-spacer">
      <?php print $content['column1']; ?>
    </div>
    <div class="hewston-col-2 hewston-col col-md-4 bpl-content bpl-spacer">
      <?php print $content['column2']; ?>
    </div>
    <div class="hewston-col-3 hewston-col col-md-4 bpl-content bpl-spacer">
      <?php print $content['column3']; ?>
    </div>
  </div>
</div>
