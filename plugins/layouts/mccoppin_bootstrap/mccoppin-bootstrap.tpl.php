<div class="mccoppin row">
  <div class="mccoppin-col-1 mccoppin-col bpl-sidebar-left bpl-sidebar col-md-4 bpl-spacer">
    <?php print $content['column1']; ?>
  </div>
  <div class="mccoppin-col-2 mccoppin-col bpl-content col-md-4 bpl-spacer">
    <?php print $content['column2']; ?>
  </div>
  <div class="mccoppin-col-3 mccoppin-col bpl-sidebar-right bpl-sidebar col-md-4 bpl-spacer">
    <?php print $content['column3']; ?>
  </div>
</div>
