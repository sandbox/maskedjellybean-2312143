<?php
// Plugin definition
$plugin = array(
  'title' => t('McCoppin'),
  'icon' => 'mccoppin.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'mccoppin_bootstrap',
  'regions' => array(
    'column1' => t('First Column'),
    'column2' => t('Second Column'),
    'column3' => t('Third Column'),
  ),
);
