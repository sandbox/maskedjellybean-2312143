<?php
// Plugin definition
$plugin = array(
  'title' => t('Webb'),
  'icon' => 'webb.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'webb_bootstrap',
  'regions' => array(
    'header' => t('Header'),
    'sidebar' => t('Sidebar'),
    'contentheader' => t('Content Header'),
    'contentcolumn1' => t('Content Column 1'),
    'contentcolumn2' => t('Content Column 2'),
  ),
);
