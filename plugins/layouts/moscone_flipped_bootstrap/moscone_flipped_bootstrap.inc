<?php
// Plugin definition
$plugin = array(
  'title' => t('Moscone Flipped'),
  'icon' => 'moscone-flipped.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'moscone_flipped_bootstrap',
  'regions' => array(
    'header' => t('Header'),
    'sidebar' => t('Content Sidebar'),
    'contentmain' => t('Content'),
    'footer' => t('Footer'),
  ),
);
