<div class="moscone">
  <div class="moscone-header row">
    <div class="moscone-col bpl-content-header col-md-12">
      <?php print $content['header']; ?>
    </div>
  </div>
  <div class="moscone-content-container row">
    <div class="moscone-content bpl-content col-md-8 bpl-spacer">
      <?php print $content['contentmain']; ?>
    </div>
    <div class="moscone-sidebar bpl-sidebar-right bpl-sidebar col-md-4 bpl-spacer">
      <?php print $content['sidebar']; ?>
    </div>
  </div>
  <div class="moscone-footer row">
    <div class="moscone-col bpl-content col-md-12">
      <?php print $content['footer']; ?>
    </div>
  </div>
</div>
