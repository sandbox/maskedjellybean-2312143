<?php
// Plugin definition
$plugin = array(
  'title' => t('Harris'),
  'icon' => 'harris.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'harris_bootstrap',
  'regions' => array(
    'header' => t('Header'),
    'column1' => t('First Column'),
    'column2' => t('Second Column'),
    'contentmain' => t('Content'),
  ),
);
