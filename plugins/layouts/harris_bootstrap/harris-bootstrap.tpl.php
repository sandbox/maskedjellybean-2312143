<?php
/**
 * @file
 * Template for Panopoly Harris.
 *
 */
?>
<div class="harris row">
  <div class="harris-header col-md-12 bpl-content-header bpl-spacer">
    <?php print $content['header']; ?>
  </div>
  <div class="harris-left-sidebar col-md-3 bpl-sidebar bpl-spacer">
    <?php print $content['column1']; ?>
  </div>
  <div class="harris-content col-md-6 bpl-content bpl-spacer">
    <?php print $content['contentmain']; ?>
  </div>
  <div class="harris-right-sidebar col-md-3 bpl-sidebar bpl-spacer">
    <?php print $content['column2']; ?>
  </div>
</div>
