<?php
// Plugin definition
$plugin = array(
  'title' => t('Jonathan Flipped'),
  'icon' => 'jonathan-flipped.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'jonathan_flipped_bootstrap',
  'regions' => array(
    'sidebar' => t('Sidebar'),
    'contentmain' => t('Content'),
  ),
);
