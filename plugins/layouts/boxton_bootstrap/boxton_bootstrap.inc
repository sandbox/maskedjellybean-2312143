<?php
// Plugin definition
$plugin = array(
  'title' => t('Boxton'),
  'icon' => 'boxton.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'boxton_bootstrap',
  'regions' => array(
    'contentmain' => t('Content'),
  ),
);
