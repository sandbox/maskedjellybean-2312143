<div class="bryant row">
  <div class="bryant-content col-md-8 bpl-content bpl-spacer">
    <?php print $content['contentmain']; ?>
  </div>
  <div class="bryant-sidebar col-md-4 bpl-sidebar bpl-spacer">
    <?php print $content['sidebar']; ?>
  </div>
</div>
