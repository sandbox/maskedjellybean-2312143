<?php
// Plugin definition
$plugin = array(
  'title' => t('Bryant Flipped'),
  'icon' => 'bryant-flipped.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'bryant_flipped_bootstrap',
  'regions' => array(
    'sidebar' => t('Sidebar'),
    'contentmain' => t('Content'),
  ),
);
