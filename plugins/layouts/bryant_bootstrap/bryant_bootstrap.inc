<?php
// Plugin definition
$plugin = array(
  'title' => t('Bryant'),
  'icon' => 'bryant.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'bryant_bootstrap',
  'regions' => array(
    'sidebar' => t('Sidebar'),
    'contentmain' => t('Content'),
  ),
);
