<?php
// Plugin definition
$plugin = array(
  'title' => t('Taylor'),
  'icon' => 'taylor.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'taylor_bootstrap',
  'regions' => array(
    'header' => t('Header'),
    'half' => t('Half Column'),
    'quarter1' => t('Quarter 1 Column'),
    'quarter2' => t('Quarter 2 Column'),
    'footer' => t('Footer'),
  ),
);
