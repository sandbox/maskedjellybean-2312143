<div class="taylor">
  <div class="taylor-top row">
    <div class="taylor-header bpl-content-header col-md-12 bpl-spacer">
      <?php print $content['header']; ?>
    </div>
  </div>
  <div class="taylor-middle row">
    <div class="taylor-col-half taylor-col bpl-content col-md-6 bpl-spacer">
      <?php print $content['half']; ?>
    </div>
    <div class="taylor-col-quarter taylor-col bpl-content col-md-3 bpl-spacer">
      <?php print $content['quarter1']; ?>
    </div>
    <div class="taylor-col-quarter taylor-col bpl-content col-md-3 bpl-spacer">
      <?php print $content['quarter2']; ?>
    </div>
  </div>
  <div class="taylor-bottom row">
    <div class="taylor-footer bpl-content col-md-12 bpl-spacer">
      <?php print $content['footer']; ?>
    </div>
  </div>
</div>
