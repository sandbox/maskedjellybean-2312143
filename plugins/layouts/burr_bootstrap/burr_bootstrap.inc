<?php
// Plugin definition
$plugin = array(
  'title' => t('Burr'),
  'icon' => 'burr.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'burr_bootstrap',
  'regions' => array(
    'sidebar' => t('Sidebar'),
    'contentmain' => t('Content'),
  ),
);
