<?php
// Plugin definition
$plugin = array(
  'title' => t('Moscone'),
  'icon' => 'moscone.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'moscone_bootstrap',
  'regions' => array(
    'header' => t('Header'),
    'sidebar' => t('Content Sidebar'),
    'contentmain' => t('Content'),
    'footer' => t('Footer'),
  ),
);
