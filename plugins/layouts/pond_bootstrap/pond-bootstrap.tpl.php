<div class="pond">
  <div class="pond-top row">
    <div class="pond-header bpl-content-header col-md-12 bpl-spacer">
      <?php print $content['header']; ?>
    </div>
  </div>
  <div class="pond-content-row-1 pond-content-row row">
    <div class="pond-col-1 pond-col bpl-content col-md-4 bpl-spacer">
      <?php print $content['column1']; ?>
    </div>
    <div class="pond-col-2 pond-col bpl-content col-md-4 bpl-spacer">
      <?php print $content['column2']; ?>
    </div>
    <div class="pond-col-3 pond-col bpl-content col-md-4 bpl-spacer">
      <?php print $content['column3']; ?>
    </div>
  </div>
  <div class="pond-content-row-2 pond-content-row row">
    <div class="pond-col-1 pond-col bpl-content col-md-4 bpl-spacer">
      <?php print $content['secondarycolumn1']; ?>
    </div>
    <div class="pond-col-2 pond-col bpl-content col-md-4 bpl-spacer">
      <?php print $content['secondarycolumn2']; ?>
    </div>
    <div class="pond-col-3 pond-col bpl-content col-md-4 bpl-spacer">
      <?php print $content['secondarycolumn3']; ?>
    </div>
  </div>
  <div class="pond-bottom row">
    <div class="pond-footer bpl-content col-md-12 bpl-spacer">
      <?php print $content['footer']; ?>
    </div>
  </div>
</div>
