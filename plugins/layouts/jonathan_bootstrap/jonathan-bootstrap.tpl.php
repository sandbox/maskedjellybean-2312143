<div class="jonathan row">
  <div class="jonathan-sidebar col-md-4 col-lg-3 bpl-sidebar bpl-spacer">
    <?php print $content['sidebar']; ?>
  </div>
  <div class="jonathan-content col-md-8 col-lg-9 bpl-content bpl-spacer">
    <?php print $content['contentmain']; ?>
  </div>
</div>
