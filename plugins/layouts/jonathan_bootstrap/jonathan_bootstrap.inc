<?php
// Plugin definition
$plugin = array(
  'title' => t('Jonathan'),
  'icon' => 'jonathan.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'jonathan_bootstrap',
  'regions' => array(
    'sidebar' => t('Sidebar'),
    'contentmain' => t('Content'),
  ),
);
