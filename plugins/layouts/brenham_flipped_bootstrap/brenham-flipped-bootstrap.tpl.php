<div class="brenham row">
  <div class="brenham-header col-md-12 bpl-content-header bpl-spacer">
    <?php print $content['header']; ?>
  </div>
  <div class="brenham-content col-md-8 bpl-content bpl-spacer">
    <?php print $content['contentmain']; ?>
  </div>
  <div class="brenham-right-sidebar col-md-4 bpl-sidebar bpl-spacer">
    <?php print $content['sidebar']; ?>
  </div>
</div>
