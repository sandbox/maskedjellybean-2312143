<?php
// Plugin definition
$plugin = array(
  'title' => t('Brenham Flipped'),
  'icon' => 'brenham-flipped.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'brenham_flipped_bootstrap',
  'regions' => array(
    'header' => t('Header'),
    'sidebar' => t('Content Sidebar'),
    'contentmain' => t('Content'),
  ),
);
