<div class="webb row">
  <div class="webb-top bpl-content-header col-md-12 bpl-spacer">
    <?php print $content['header']; ?>
  </div>
  <div class="webb-content-container col-md-8">
    <div class="webb-content row">
      <div class="webb-content-top bpl-content col-md-12 bpl-spacer">
        <?php print $content['contentheader']; ?>
      </div>
      <div class="webb-col-1 webb-col bpl-content col-md-6 bpl-spacer">
        <?php print $content['contentcolumn1']; ?>
      </div>
      <div class="webb-col-2 webb-col bpl-content col-md-6 bpl-spacer">
        <?php print $content['contentcolumn2']; ?>
      </div>
    </div>
  </div>
  <div class="webb-sidebar bpl-sidebar-right bpl-sidebar col-md-4 bpl-spacer">
    <?php print $content['sidebar']; ?>
  </div>
</div>
