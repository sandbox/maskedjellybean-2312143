<?php
// Plugin definition
$plugin = array(
  'title' => t('Rolph'),
  'icon' => 'rolph.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'rolph_bootstrap',
  'regions' => array(
    'header' => t('Header'),
    'quarter1' => t('Quarter 1 Column'),
    'quarter2' => t('Quarter 2 Column'),
    'quarter3' => t('Quarter 1 Column'),
    'quarter4' => t('Quarter 2 Column'),
    'footer' => t('Footer'),
  ),
);
