<div class="rolph">
  <div class="rolph-top row">
    <div class="rolph-header bpl-content-header col-md-12 bpl-spacer">
      <?php print $content['header']; ?>
    </div>
  </div>
  <div class="rolph-middle row">
    <div class="rolph-col-1 rolph-col bpl-content col-md-3 bpl-spacer">
      <?php print $content['quarter1']; ?>
    </div>
    <div class="rolph-col-2 rolph-col bpl-content col-md-3 bpl-spacer">
      <?php print $content['quarter2']; ?>
    </div>
    <div class="rolph-col-3 rolph-col bpl-content col-md-3 bpl-spacer">
      <?php print $content['quarter3']; ?>
    </div>
    <div class="rolph-col-4 rolph-col bpl-content col-md-3 bpl-spacer">
      <?php print $content['quarter4']; ?>
    </div>
  </div>
  <div class="rolph-bottom row">
    <div class="rolph-footer bpl-content col-md-12 bpl-spacer">
      <?php print $content['footer']; ?>
    </div>
  </div>
</div>
