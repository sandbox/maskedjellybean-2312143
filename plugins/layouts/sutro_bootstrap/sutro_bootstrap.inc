<?php
// Plugin definition
$plugin = array(
  'title' => t('Sutro'),
  'icon' => 'sutro.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'sutro_bootstrap',
  'regions' => array(
    'header' => t('Header'),
    'column1' => t('First Column'),
    'column2' => t('Second Column'),
    'footer' => t('Footer'),
  ),
);
