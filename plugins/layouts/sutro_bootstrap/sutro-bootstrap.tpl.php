<div class="sutro">
  <div class="sutro-top row">
    <div class="sutro-header bpl-content-header col-md-12 bpl-spacer">
      <?php print $content['header']; ?>
    </div>
  </div>
  <div class="sutro-middle row">
    <div class="sutro-col-1 sutro-col bpl-content col-md-6 bpl-spacer">
      <?php print $content['column1']; ?>
    </div>
    <div class="sutro-col-2 sutro-col bpl-content col-md-6 bpl-spacer">
      <?php print $content['column2']; ?>
    </div>
  </div>
  <div class="sutro-bottom row">
    <div class="sutro-footer bpl-content col-md-12 bpl-spacer">
      <?php print $content['footer']; ?>
    </div>
  </div>
</div>
