<?php
// Plugin definition
$plugin = array(
  'title' => t('Geary'),
  'icon' => 'geary.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'geary_bootstrap',
  'regions' => array(
    'header' => t('Header'),
    'column1' => t('First Column'),
    'column2' => t('Second Column'),
    'column3' => t('Third Column'),
  ),
);
