<div class="geary row">
  <div class="geary-header col-md-12 bpl-content-header bpl-spacer">
    <?php print $content['header']; ?>
  </div>
  <div class="geary-col-1 geary-col col-md-4 bpl-content bpl-spacer">
    <?php print $content['column1']; ?>
  </div>
  <div class="geary-col-2 geary-col col-md-4 bpl-content bpl-spacer">
    <?php print $content['column2']; ?>
  </div>
  <div class="geary-col-3 geary-col col-md-4 bpl-content bpl-spacer">
    <?php print $content['column3']; ?>
  </div>
</div>
