<div class="bartlett row">
  <div class="bartlett-left-sidebar col-md-4 bpl-sidebar bpl-spacer"><?php print $content['sidebar']; ?></div>
  <div class="bartlett-content-container col-md-8 bpl-content bpl-spacer">
    <div class="bartlett-content-header">
      <?php print $content['contentheader']; ?>
    </div>
    <div class="bartlett-content row">
      <div class="bartlett-content-left col-md-6 bpl-spacer">
        <?php print $content['contentcolumn1']; ?>
      </div>
      <div class="bartlett-content-right col-md-6 bpl-spacer">
        <?php print $content['contentcolumn2']; ?>
      </div>
    </div>
  </div>
</div>
