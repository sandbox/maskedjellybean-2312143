<div class="brown">
  <div class="brown-top row bpl-content-header">
    <div class="brown-slider col-md-8 bpl-spacer">
      <?php print $content['slider']; ?>
    </div>
    <div class="brown-slider-gutter col-md-4 bpl-spacer">
      <?php print $content['slidergutter']; ?>
    </div>
  </div>
  <div class="brown-middle row bpl-content">
    <div class="brown-col-1 brown-col col-md-4 bpl-spacer">
      <?php print $content['column1']; ?>
    </div>
    <div class="brown-col-2 brown-col col-md-4 bpl-spacer">
      <?php print $content['column2']; ?>
    </div>
    <div class="brown-col-3 brown-col col-md-4 bpl-spacer">
      <?php print $content['column3']; ?>
    </div>
  </div>
  <div class="brown-bottom row bpl-content">
    <div class="brown-col-1 brown-col col-md-4 bpl-spacer">
      <?php print $content['footercolumn1']; ?>
    </div>
    <div class="brown-col-2 brown-col col-md-4 bpl-spacer">
      <?php print $content['footercolumn2']; ?>
    </div>
    <div class="brown-col-3 brown-col col-md-4 bpl-spacer">
      <?php print $content['footercolumn3']; ?>
    </div>
  </div>
</div>
