<div class="burr row">
  <div class="burr-content col-md-7 bpl-content bpl-spacer">
    <?php print $content['contentmain']; ?>
  </div>
  <div class="burr-sidebar col-md-5 bpl-sidebar bpl-spacer">
    <?php print $content['sidebar']; ?>
  </div>
</div>
