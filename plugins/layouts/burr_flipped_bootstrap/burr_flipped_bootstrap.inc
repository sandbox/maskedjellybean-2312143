<?php
// Plugin definition
$plugin = array(
  'title' => t('Burr Flipped'),
  'icon' => 'burr-flipped.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'burr_flipped_bootstrap',
  'regions' => array(
    'sidebar' => t('Sidebar'),
    'contentmain' => t('Content'),
  ),
);
