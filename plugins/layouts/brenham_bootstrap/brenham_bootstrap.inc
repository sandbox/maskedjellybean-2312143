<?php
// Plugin definition
$plugin = array(
  'title' => t('Brenham'),
  'icon' => 'brenham.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'brenham_bootstrap',
  'regions' => array(
    'header' => t('Header'),
    'sidebar' => t('Content Sidebar'),
    'contentmain' => t('Content'),
  ),
);
