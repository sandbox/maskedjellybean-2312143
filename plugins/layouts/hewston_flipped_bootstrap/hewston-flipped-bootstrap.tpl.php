<?php
/**
 * @file
 * Template for default Panopoly Hewston Flipped.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
?>
<div class="hewston">
  <div class="hewston-top row">
    <div class="hewston-slider-gutter col-md-4 bpl-content-header bpl-spacer">
      <?php print $content['slidergutter']; ?>
    </div>
    <div class="hewston-slider col-md-8 bpl-content-header bpl-spacer">
      <?php print $content['slider']; ?>
    </div>
  </div>
  <div class="hewston-middle row">
    <div class="hewston-col-1 hewston-col col-md-4 bpl-content bpl-spacer">
      <?php print $content['column1']; ?>
    </div>
    <div class="hewston-col-2 hewston-col col-md-4 bpl-content bpl-spacer">
      <?php print $content['column2']; ?>
    </div>
    <div class="hewston-col-3 hewston-col col-md-4 bpl-content bpl-spacer">
      <?php print $content['column3']; ?>
    </div>
  </div>
</div>
