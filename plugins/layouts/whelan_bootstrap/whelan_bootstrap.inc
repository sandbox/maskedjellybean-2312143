<?php
// Plugin definition
$plugin = array(
  'title' => t('Whelan'),
  'icon' => 'whelan.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'whelan_bootstrap',
  'regions' => array(
    'column1' => t('First Column'),
    'column2' => t('Second Column'),
    'contentmain' => t('Content'),
  ),
);
