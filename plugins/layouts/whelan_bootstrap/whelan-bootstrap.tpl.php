<div class="whelan row">
  <div class="whelan-col-1 whelan-col bpl-sidebar-left bpl-sidebar col-md-3 bpl-spacer">
    <?php print $content['column1']; ?>
  </div>
  <div class="whelan-col-2 whelan-col bpl-content col-md-6 bpl-spacer">
    <?php print $content['contentmain']; ?>
  </div>
  <div class="whelan-col-3 whelan-col bpl-sidebar-right bpl-sidebar col-md-3 bpl-spacer">
    <?php print $content['column2']; ?>
  </div>
</div>
