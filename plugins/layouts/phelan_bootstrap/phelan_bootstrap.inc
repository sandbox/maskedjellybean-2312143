<?php
// Plugin definition
$plugin = array(
  'title' => t('Phelan'),
  'icon' => 'phelan.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'phelan_bootstrap',
  'regions' => array(
    'column1' => t('First Column'),
    'column2' => t('Second Column'),
  ),
);
