<div class="phelan row">
  <div class="phelan-col-1 phelan-col bpl-content col-md-6 bpl-spacer">
    <?php print $content['column1']; ?>
  </div>
  <div class="phelan-col-2 phelan-col bpl-content col-md-6 bpl-spacer">
    <?php print $content['column2']; ?>
  </div>
</div>
