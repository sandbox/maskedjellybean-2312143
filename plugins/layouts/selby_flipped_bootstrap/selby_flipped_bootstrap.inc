<?php
// Plugin definition
$plugin = array(
  'title' => t('Selby Flipped'),
  'icon' => 'selby-flipped.png',
  'category' => t('Bootstrap Panopoly Layouts'),
  'theme' => 'selby_flipped_bootstrap',
  'regions' => array(
    'sidebar' => t('Sidebar'),
    'contentheader' => t('Content Header'),
    'contentcolumn1' => t('Content Column 1'),
    'contentcolumn2' => t('Content Column 2'),
    'contentfooter' => t('Content Footer'),
  ),
);
