<div class="selby row">
  <div class="selby-content col-md-8 bpl-spacer">
    <div class="selby-top row">
      <div class="selby-header bpl-content col-md-12 bpl-spacer">
        <?php print $content['contentheader']; ?>
      </div>
    </div>
    <div class="selby-middle row">
      <div class="selby-col-1 selby-col bpl-content col-md-6 bpl-spacer">
        <?php print $content['contentcolumn1']; ?>
      </div>
      <div class="selby-col-2 selby-col bpl-content col-md-6 bpl-spacer">
        <?php print $content['contentcolumn2']; ?>
      </div>
    </div>
    <div class="selby-bottom row">
      <div class="selby-footer bpl-content col-md-12 bpl-spacer">
        <?php print $content['contentfooter']; ?>
      </div>
    </div>
  </div>
  <div class="selby-sidebar bpl-sidebar-right bpl-sidebar col-md-4 bpl-spacer">
    <?php print $content['sidebar']; ?>
  </div>
</div>
