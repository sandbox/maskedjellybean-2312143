<?php
/**
 * @file
 * Template for Panopoly Sanderson.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
?>
<div class="sanderson">
  <div class="sanderson-content-row-1 sanderson-content-row row">
    <div class="sanderson-col-1 sanderson-col bpl-content col-md-6 bpl-spacer">
      <?php print $content['column1']; ?>
    </div>
    <div class="sanderson-col-2 sanderson-col bpl-content col-md-6 ol-space">
      <?php print $content['column2']; ?>
    </div>
  </div>
  <div class="sanderson-content-row-2 sanderson-content-row row">
    <div class="sanderson-col-1 sanderson-col bpl-content col-md-4 bpl-spacer">
      <?php print $content['secondarycolumn1']; ?>
    </div>
    <div class="sanderson-col-2 sanderson-col bpl-content col-md-4 bpl-spacer">
      <?php print $content['secondarycolumn2']; ?>
    </div>
    <div class="sanderson-col-3 sanderson-col bpl-content col-md-4 bpl-spacer">
      <?php print $content['secondarycolumn3']; ?>
    </div>
  </div>
</div>
